# JITSI CLOUD

Multicloud Jitsi video conference VM deployment and configuration.

---

## Usage

---

### Ansible Only

- Copy Jitsi settings

```bash
cd ansible
cp group_vars/vm/jitsi_user.yaml.example group_vars/vm/jitsi_user.yaml
```

- Edit the file

#### Scaleway

- Copy Scaleway settings

```bash
cd ansible
cp group_vars/local/scaleway_user.yaml.example group_vars/local/scaleway_user.yaml
```

- Edit the file

- Launch the playbook:

```bash
# Create VM
ansible-playbook jitsi_cloud.yaml -t scaleway,configuration
```

#### Google Cloud Platform

- Copy GCP settings

```bash
cd ansible
cp group_vars/local/gcp_user.yaml.example group_vars/local/gcp_user.yaml
```

- Edit the file

- Launch the playbook:

```bash
# Create VM
ansible-playbook jitsi_cloud.yaml -t gcp,configuration
```

---

### Terraform + Ansible

- Copy Jitsi settings

```bash
cd ansible
cp group_vars/vm/jitsi_user.yaml.example group_vars/vm/jitsi_user.yaml
```

- Edit the file

#### Scaleway

- Follow [Scaleway Terraform README](terraform/scaleway/README.md) instructions

- Launch Ansible playbook

```bash
cd ansible
ansible-playbook jitsi_cloud.yaml -t configuration
```

#### Google Cloud Platform

- Follow [GCP Terraform README](terraform/gcp/README.md) instructions

- Launch Ansible playbook

```bash
cd ansible
ansible-playbook jitsi_cloud.yaml -t configuration
```

---

## Cleaning

### Ansible Only

#### Scaleway

- Launch the playbook:

```bash
cd ansible
ansible-playbook destroy.yaml -t scaleway
```

#### Google Cloud Platform

- Launch the playbook:

```bash
cd ansible
ansible-playbook destroy.yaml -t gcp
```

---

### Terraform + Ansible

- Delete VM IP address in `inventory.ini`

```bash
sed -i /ansible_user/d ansible/inventory.ini
```

#### Scaleway

- Destroy Scaleway infrastructure

```bash
cd terraform/scaleway
terraform destroy -auto-approve
```

#### Google Cloud Platform

- Destroy GCP infrastructure

```bash
cd terraform/gcp
terraform destroy -auto-approve
```
