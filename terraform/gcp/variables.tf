variable "project" { }

variable "credentials_file" { }

variable "region" {
  default = "europe-west1"
}

variable "zone" {
  default = "europe-west1-b"
}

variable "environment" {
  type    = string
  default = "prod"
}

variable "machine_types" {
  type    = map
  default = {
    dev  = "f1-micro"
    prod = "e2-medium"
  }
}

variable "vm_name" {
  default = "jitsivm"
}

variable "vm_user" {
  default = "ubuntu"
}
