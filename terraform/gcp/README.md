# Jitsi Cloud GCP with Terraform

## Usage

- Copy Terraform variables file

```bash
cp terraform/gcp/terraform.tfvars.example terraform/gcp/terraform.tfvars
```

- Edit the file with the proper values

- Apply Terraform

```bash
terraform init
terraform apply -auto-approve
```
