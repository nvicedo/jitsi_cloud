resource "google_compute_instance" "vm_instance" {
  name         = var.vm_name
  machine_type = var.machine_types[var.environment]
  tags = [ "jitsi" ]

  provisioner "local-exec" {
    command = "echo '${google_compute_instance.vm_instance.network_interface[0].access_config[0].nat_ip} ansible_user=${var.vm_user} ' >> ../../ansible/inventory.ini"
  }

  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-2004-lts"
    }
  }

  network_interface {
    network = "default"
    access_config {
      nat_ip = google_compute_address.vm_static_ip.address
    }
  }
}
