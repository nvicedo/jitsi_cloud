# Jitsi Cloud Scaleway with Terraform

## Usage

- Copy Terraform variables file

```bash
cp terraform/scaleway/terraform.tfvars.example terraform/scaleway/terraform.tfvars
```

- Edit the file with the proper values

- Apply Terraform

```bash
terraform init
terraform apply -auto-approve
```
