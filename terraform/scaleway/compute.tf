resource "scaleway_instance_server" "vm_instance" {
  name = var.vm_name
  type = var.machine_types[var.environment]
  tags = [ "jitsi" ]

  provisioner "local-exec" {
    command = "echo '${scaleway_instance_ip.public_ip.address} ansible_user=${var.vm_user} ' >> ../../ansible/inventory.ini"
  }

  image = "ubuntu_focal"

  ip_id = scaleway_instance_ip.public_ip.id

  security_group_id = scaleway_instance_security_group.terraform_fw.id
}
