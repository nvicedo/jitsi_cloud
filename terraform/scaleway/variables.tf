variable "organization_id" {
  default = "00000000-1111-2222-3333-444444444444"
}

variable "access_key" {
  default = "AAAAAAAAAAAAAAAAAAAA"
}

variable "secret_key" {
  default = "00000000-1111-2222-3333-444444444444"
}

variable "region" {
  default = "fr-par"
}

variable "zone" {
  default = "fr-par-1"
}

variable "environment" {
  type    = string
  default = "prod"
}

variable "machine_types" {
  type    = map
  default = {
    dev  = "STARDUST1-S"
    prod = "DEV1-M"
  }
}

variable "vm_name" {
  default = "jitsivm"
}

variable "vm_user" {
  default = "ubuntu"
}
